from random import randint
from subprocess import call

output = open("bingo.tex","w")
output.write("\\documentclass{scrartcl}\n"+
             "\\usepackage[ngerman]{babel}\n"+
             "\\usepackage[german=guillemets]{csquotes}\n"+
             "\\usepackage[margin=.75cm]{geometry}\n"+
             "\n"+
             "\\date{\\today}\n"+
             "\\title{Bullshit-Bingo}\n"+
             "\\author{Automatisch Generiert}\n"+
             "\\begin{document}\n"+
             "\\maketitle\n"+
             "\\begin{center}\n"+
             "\\begin{tabular}{|c|c|c|c|c|}\n"+
             "\\hline\n")
read_data=[]
with open('begriffe.txt', 'r') as f:
    for line in f:
        read_data.append(line.rstrip())
print(read_data)
for i in range(0,5):
    for ii in range(0,5):
        if(ii>0):
            output.write("&")
        index = randint(0,len(read_data)-1)
        output.write("\\parbox[0pt][.15\\textwidth][c]{.15\\textwidth}{"+(read_data[index])+"}\n")
        read_data.remove(read_data[index])
    output.write("\\\\\n"+
                 "\\hline\n")

output.write("\\end{tabular}\n"+
             "\\end{center}\n"+
             "\\end{document}")
output.close()
call(["xelatex", "bingo.tex"])
